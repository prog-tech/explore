FROM ubuntu:22.04
LABEL authors="taekwandodo"

RUN    apt update \
    && apt install -y \
            git \
            ruby \
    && apt clean

RUN useradd -s /bin/bash -m -G sudo seminar

WORKDIR /home/seminar

COPY createRepo.sh ./create-repo-without-git/create.sh

RUN    chown -R seminar /home/seminar \
    && chmod +x create-repo-without-git/create.sh

USER seminar

ENTRYPOINT ["top", "-b"]
