# Create git repo without git

1. `CONTAINER_ID=$(docker run -d registry.gitlab.com/prog-tech/explore:basics-0.0.0)`
2. `docker exec $CONTAINER_ID bash`
3. `cd create-repo-without-git`
4. `git status`
5. `./create.sh`
6. `git status`
7. `git add create.sh`
8. go to `.git` directory and research:
   1. see `index` file - [details here](https://git-scm.com/docs/index-format)
   2. go to `objects` and explore its files with `git cat-file -p [hash]`