#! /bin/bash

mkdir .git && cd .git # create '.git' directory & change directory to '.git'

echo "ref: refs/heads/main" >> HEAD # create 'HEAD' file
mkdir objects # create 'objects' directory
mkdir refs # create 'refs' directory

cd ..
