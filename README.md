# git
> Programming Technologies course.

## How to run examples?

1. Install and start docker
2. Run container:
    > registry.gitlab.com/prog-tech/explore:basics-0.0.0
3. Enter container and follow instructions

We suggest following these steps:
* `CONTAINER_ID=$(docker run -d <image name>)`
* `docker exec -it $CONTAINER_ID bash`
