# Commit naming

* New topic: `[ TOPIC ] topic name`
* Fix: `[ FIX ] description`
* Removal: `[ REMOVAL ] description`
* Additional information, etc: `[ IMPROVEMENT ] description`
* Infrastructure: `[ INFRA ] description`
* Docker images: `[ DOCKER ] description`
* Restyling: `[ STYLE ] description`
